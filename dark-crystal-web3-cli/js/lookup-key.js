const sodium = require('sodium-javascript')

// Generate the first lookup key used in the example to check
// compatibility of js and rust blake2b keyed hash

const pk = Buffer.from('cf61ca2d786bc57cff2455003862ee70ca64d0dddb8b6ae0d1927e81411bdd21', 'hex')
const lookup = Buffer.from('Some lookup key')

const lookupKey = Buffer.alloc(32)
sodium.crypto_generichash(lookupKey, lookup, pk)

console.log(lookupKey.toString('hex'))
