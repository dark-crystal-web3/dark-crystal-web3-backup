//! Encode secrets as various types

use crate::secret::bip39::{bytes_to_mnemonic, mnemonic_to_bytes};
use std::str::Utf8Error;
pub mod bip39;

/// A secret with the first byte representing its type
/// allowing for up to 256 types
#[derive(Debug, PartialEq)]
pub enum Secret {
    Raw(Vec<u8>),  // 0
    Utf8(String),  // 1
    Bip39(String), // 2
}
// TODO Labelled(String, Vec<u8>), // 3

impl Secret {
    /// Given an encoded secret, determine its type
    pub fn from_bytes(input: Vec<u8>) -> Result<Secret, EncodingError> {
        match input[0] {
            0 => Ok(Secret::Raw(input[1..].to_vec())),
            1 => Ok(Secret::Utf8(std::str::from_utf8(&input[1..])?.to_string())),
            2 => Ok(Secret::Bip39(bytes_to_mnemonic(&input[1..].to_vec())?)),
            _ => Err(EncodingError {}),
        }
    }

    /// Encode a secret
    pub fn to_bytes(&self) -> Result<Vec<u8>, EncodingError> {
        fn add_type(type_code: u8, input: Vec<u8>) -> Vec<u8> {
            let mut output: Vec<u8> = vec![type_code];
            output.extend(input);
            output
        }

        match self {
            Secret::Raw(bytes) => Ok(add_type(0, bytes.to_vec())),
            Secret::Utf8(string) => Ok(add_type(1, string.as_bytes().to_vec())),
            Secret::Bip39(string) => Ok(add_type(2, mnemonic_to_bytes(string)?)),
        }
    }

    pub fn detect_from_string(input: String) -> Secret {
        match mnemonic_to_bytes(&input) {
            Ok(_) => Secret::Bip39(input),
            Err(_) => match hex::decode(&input) {
                Ok(bytes) => Secret::Raw(bytes),
                Err(_) => Secret::Utf8(input),
            },
        }
    }
}

/// Error given when encoding or decoding a secret
#[derive(Debug)]
pub struct EncodingError {}

impl From<Utf8Error> for EncodingError {
    fn from(_error: std::str::Utf8Error) -> Self {
        EncodingError {}
    }
}

impl From<::bip39::Error> for EncodingError {
    fn from(_error: ::bip39::Error) -> Self {
        EncodingError {}
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn encode_secrets() {
        assert_eq!(
            vec![0, 98, 108, 97, 104],
            Secret::Raw(b"blah".to_vec()).to_bytes().unwrap()
        );

        assert_eq!(
            vec![1, 98, 108, 97, 104],
            Secret::Utf8("blah".to_string()).to_bytes().unwrap()
        );

        assert_eq!(
            vec![2, 208, 109, 216, 221, 151, 58, 13, 74, 112, 181, 62, 174, 181, 203, 225, 153],
            Secret::Bip39(
                "spare human danger common patch pioneer security pond push purity wear crane"
                    .to_string()
            )
            .to_bytes()
            .unwrap()
        );
    }

    #[test]
    fn decode_secrets() {
        assert_eq!(
            Secret::from_bytes(vec![0, 98, 108, 97, 104]).unwrap(),
            Secret::Raw(b"blah".to_vec())
        );

        assert_eq!(
            Secret::from_bytes(vec![1, 98, 108, 97, 104]).unwrap(),
            Secret::Utf8("blah".to_string())
        );

        assert_eq!(
            Secret::from_bytes(vec![
                2, 208, 109, 216, 221, 151, 58, 13, 74, 112, 181, 62, 174, 181, 203, 225, 153
            ])
            .unwrap(),
            Secret::Bip39(
                "spare human danger common patch pioneer security pond push purity wear crane"
                    .to_string()
            )
        );
    }

    #[test]
    fn detect_string() {
        assert_eq!(
            Secret::detect_from_string("blah".to_string()),
            Secret::Utf8("blah".to_string())
        );

        assert_eq!(
            Secret::detect_from_string("deadbeef".to_string()),
            Secret::Raw(hex::decode("deadbeef").unwrap())
        );

        assert_eq!(
            Secret::detect_from_string("dead beef".to_string()),
            Secret::Utf8("dead beef".to_string())
        );

        assert_eq!(
            Secret::detect_from_string(
                "spare human danger common patch pioneer security pond push purity wear crane"
                    .to_string()
            ),
            Secret::Bip39(
                "spare human danger common patch pioneer security pond push purity wear crane"
                    .to_string()
            )
        );
    }
}
