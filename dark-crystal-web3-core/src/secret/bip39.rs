//! A wrapper around the bip39 crate allowing us to
//! validate and efficiently encode secrets given as a seed phrase
pub use bip39::Error;
use bip39::{Language, Mnemonic};

/// Convert a seed phrase to a vector of bytes
pub fn mnemonic_to_bytes(phrase: &str) -> Result<Vec<u8>, Error> {
    let mnemonic = Mnemonic::parse_in_normalized(Language::English, phrase)?;
    Ok(mnemonic.to_entropy())
}

/// Converts a vector of bytes to a seed phrase
pub fn bytes_to_mnemonic(input: &Vec<u8>) -> Result<String, Error> {
    let mnemonic = Mnemonic::from_entropy_in(Language::English, input)?;
    Ok(mnemonic.to_string())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn words_24() {
        let input = "help avocado december frown result general supply meadow market alley manual dynamic balance turkey envelope sand patient imitate unit burst swamp modify lion correct";
        let encoded = mnemonic_to_bytes(&input).unwrap();
        assert_eq!(input, bytes_to_mnemonic(&encoded).unwrap());
    }

    #[test]
    fn words_12() {
        let input = "spare human danger common patch pioneer security pond push purity wear crane";
        let encoded = mnemonic_to_bytes(&input).unwrap();
        assert_eq!(input, bytes_to_mnemonic(&encoded).unwrap());
    }
}
