//! Create encrypted share payloads, which are encoded into links
//! to the dark-crystal-web3 dapp. Recover a secret from a set
//! of decrypted shares.

use core::num::TryFromIntError;
use cryptoxide::{blake2b::Blake2b, mac::Mac};
use dark_crystal_key_backup_rust::ShareAndEncryptError;
pub use dark_crystal_key_backup_rust::{
    combine_authenticated, default_threshold, share_and_encrypt_detached_nonce,
    share_authenticated, EncryptedShareSet, Error, RecoveryError,
};
use hex;
use serde::{Deserialize, Serialize};
use serde_qs;
use std::collections::HashMap;
use std::collections::HashSet;
use std::fmt;

pub mod secret;

const DAPP_URL: &str = "https://web3.darkcrystal.pw/store";

pub const SHARE_LENGTH: usize = 33;

/// Encrypted share payload as hex strings
/// Field names are abbreviated to keep link short
#[derive(Serialize, Deserialize, Debug)]
struct ShareOutput {
    /// Ephemeral public key
    pk: String,
    /// Ciphertext
    c: String,
    /// Shares array
    s: Vec<String>,
    /// Lookup keys array
    lk: Vec<String>,
}

/// Encode share payload as hex strings
fn parse_encrypted_shares(
    encrypted_share_set: EncryptedShareSet,
    lookup_keys: Vec<Vec<u8>>,
) -> ShareOutput {
    ShareOutput {
        pk: hex::encode(encrypted_share_set.eph_public_key.as_bytes()),
        c: hex::encode(&encrypted_share_set.ciphertext),
        s: encrypted_share_set
            .encrypted_shares
            .iter()
            .map(|share| hex::encode(share))
            .collect(),
        lk: lookup_keys.iter().map(|k| hex::encode(k)).collect(),
    }
}

/// Build a URL from the given share payload
fn build_url(share_output: &ShareOutput) -> String {
    format!(
        "{}?{}",
        DAPP_URL,
        serde_qs::to_string(share_output).unwrap() // TODO
    )
}

/// Error when creating shares
#[derive(Debug)]
pub struct ShareError {
    pub message: String,
}

impl fmt::Display for ShareError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Error when creating shares {}", self.message)
    }
}

impl From<Error> for ShareError {
    fn from(error: Error) -> Self {
        ShareError {
            message: error.to_string(),
        }
    }
}

impl From<ShareAndEncryptError> for ShareError {
    fn from(error: ShareAndEncryptError) -> Self {
        ShareError {
            message: error.to_string(),
        }
    }
}

impl From<TryFromIntError> for ShareError {
    fn from(_error: TryFromIntError) -> Self {
        ShareError {
            message: "It is not possible to create more than 256 shares".to_string(),
        }
    }
}

/// Hash recovery partner's public keys together with the lookup
/// key to create a lookup key for each share
fn generate_lookup_keys(lookup_key: Vec<u8>, public_keys: &Vec<[u8; 32]>) -> Vec<Vec<u8>> {
    public_keys
        .iter()
        .map(|pk| {
            let mut context = Blake2b::new_keyed(32, &pk.clone());
            context.input(&lookup_key);
            context.result().code().to_vec()
        })
        .collect()
}

/// Create an encrypted share payload link
pub fn share(
    public_keys: Vec<[u8; 32]>,
    secret: Vec<u8>,
    lookup_key: Vec<u8>,
) -> Result<String, ShareError> {
    let threshold = default_threshold(public_keys.len().try_into()?);
    let lookup_keys = generate_lookup_keys(lookup_key, &public_keys);
    let encrypted_share_set = share_and_encrypt_detached_nonce(public_keys, secret, threshold)?;
    Ok(build_url(&parse_encrypted_shares(
        encrypted_share_set,
        lookup_keys,
    )))
}

/// A plaintext share together with associated ciphertext
#[derive(Debug)]
pub struct RecoveredShare {
    share: Vec<u8>,
    ciphertext: Vec<u8>,
}

impl RecoveredShare {
    pub fn from_concat(share_and_ciphertext: &Vec<u8>) -> Self {
        // TODO handle the case where share_and_ciphertext.len() < SHARE_LENGTH
        RecoveredShare {
            share: (&share_and_ciphertext[..SHARE_LENGTH]).to_vec(),
            ciphertext: (&share_and_ciphertext[SHARE_LENGTH..]).to_vec(),
        }
    }
}

impl fmt::Display for RecoveredShare {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}{}",
            hex::encode(&self.share),
            hex::encode(&self.ciphertext)
        )
    }
}

/// Attempt recovery for a set of shares, handling the case that
/// shares from different share sets are mixed together,
/// and also handling duplicate shares.
/// This will only return the first secret found. If there is more
/// than one complete set, multiple secrets will not be recovered
pub fn recover(recovered_shares: Vec<RecoveredShare>) -> Result<Vec<u8>, RecoveryError> {
    let mut share_sets: HashMap<Vec<u8>, HashSet<Vec<u8>>> = HashMap::new();
    let mut last_error = RecoveryError {
        message: "No shares".to_string(),
    };
    for recovered_share in recovered_shares.iter() {
        let share_set = share_sets
            .entry(recovered_share.ciphertext.clone())
            .or_insert(HashSet::new());
        share_set.insert(recovered_share.share.clone());
    }

    // For each shareset with > 1 share, attempt recovery
    for (ciphertext, share_set) in share_sets.iter() {
        if share_set.len() > 1 {
            // TODO this is probably not the best way to convert HashSet to vector
            let share_set_vec: Vec<Vec<u8>> = share_set.into_iter().map(|s| s.clone()).collect();
            // if let Ok(secret) = combine_authenticated(share_set_vec, ciphertext.to_vec()) {
            //     return Ok(secret);
            // }
            match combine_authenticated(share_set_vec, ciphertext.to_vec()) {
                Ok(secret) => return Ok(secret),
                Err(error) => last_error = error,
            }
        }
    }

    Err(RecoveryError {
        message: match share_sets.len() {
            0 | 1 => last_error.message,
            _ => format!(
                "Shares from {} different sets. {}",
                share_sets.len(),
                last_error.message
            ),
        },
    })
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn create_share_payload() {
        let public_keys = vec![
            "cf61ca2d786bc57cff2455003862ee70ca64d0dddb8b6ae0d1927e81411bdd21",
            "d92d16240856d9fa136712942943e0869e7d7b93e8b3417ee4cf20946e042b0e",
        ]
        .iter()
        .map(|pk| hex::decode(pk).unwrap().try_into().unwrap())
        .collect();

        // TODO parse the querystring rather than just checking its length
        assert_eq!(
            share(
                public_keys,
                b"the treasure is under the bed".to_vec(),
                b"thirty two bytesthirty two bytes".to_vec()
            )
            .unwrap()
            .len(),
            593
        )
    }

    #[test]
    fn recover_secret() {
        let original_secret = b"hello";
        let (shares, ciphertext) = share_authenticated(&original_secret[..], 5, 3).unwrap();
        let mut recovered_shares: Vec<RecoveredShare> = Vec::new();
        for share in shares.iter() {
            recovered_shares.push(RecoveredShare {
                share: share.to_vec(),
                ciphertext: ciphertext.clone(),
            })
        }
        assert_eq!(recover(recovered_shares).unwrap(), b"hello");
    }

    #[test]
    fn recover_secret_with_mixed_shares() {
        let original_secret = b"hello";
        let (shares, ciphertext) = share_authenticated(&original_secret[..], 5, 3).unwrap();

        let another_secret = b"flip flop";
        let (other_shares, other_ciphertext) =
            share_authenticated(&another_secret[..], 5, 3).unwrap();

        let mut recovered_shares: Vec<RecoveredShare> = Vec::new();
        recovered_shares.push(RecoveredShare {
            share: other_shares[1].to_vec(),
            ciphertext: other_ciphertext.clone(),
        });
        for share in shares.iter().take(3) {
            recovered_shares.push(RecoveredShare {
                share: share.to_vec(),
                ciphertext: ciphertext.clone(),
            })
        }
        recovered_shares.push(RecoveredShare {
            share: other_shares[2].to_vec(),
            ciphertext: other_ciphertext.clone(),
        });
        assert_eq!(recover(recovered_shares).unwrap(), b"hello");
    }

    #[test]
    fn recover_from_hex_strings() {
        let recovered_share_strings = vec![
            "015b8f00d950afdc48570f8d091eb6df04e0d7b08bfea5d8b1ed0c77ccebe705219f24e166cc5ad44ed5804c094783734e18ed12ac3169ab493c7bc96152f29800daba4c58360d886d5ff24d3f8d",
            "03ad5048bda782df8bad8ca4978ff9c8f1031db045dc53dda176702883e84619b99f24e166cc5ad44ed5804c094783734e18ed12ac3169ab493c7bc96152f29800daba4c58360d886d5ff24d3f8d",
            "04e4442cb6b5344904cf0b56a405683313f789299741f82858d986407f5a1f784d9f24e166cc5ad44ed5804c094783734e18ed12ac3169ab493c7bc96152f29800daba4c58360d886d5ff24d3f8d"
        ];
        let mut recovered_shares: Vec<RecoveredShare> = Vec::new();
        for recovered_share_string in recovered_share_strings.iter() {
            recovered_shares.push(RecoveredShare::from_concat(
                &hex::decode(recovered_share_string).unwrap(),
            ));
        }
        assert_eq!(recover(recovered_shares).unwrap(), b"hello");
    }

    #[test]
    fn ignore_duplicate_share() {
        let recovered_share_strings = vec![
            "015b8f00d950afdc48570f8d091eb6df04e0d7b08bfea5d8b1ed0c77ccebe705219f24e166cc5ad44ed5804c094783734e18ed12ac3169ab493c7bc96152f29800daba4c58360d886d5ff24d3f8d",
            "015b8f00d950afdc48570f8d091eb6df04e0d7b08bfea5d8b1ed0c77ccebe705219f24e166cc5ad44ed5804c094783734e18ed12ac3169ab493c7bc96152f29800daba4c58360d886d5ff24d3f8d",
            "03ad5048bda782df8bad8ca4978ff9c8f1031db045dc53dda176702883e84619b99f24e166cc5ad44ed5804c094783734e18ed12ac3169ab493c7bc96152f29800daba4c58360d886d5ff24d3f8d",
            "04e4442cb6b5344904cf0b56a405683313f789299741f82858d986407f5a1f784d9f24e166cc5ad44ed5804c094783734e18ed12ac3169ab493c7bc96152f29800daba4c58360d886d5ff24d3f8d"
        ];
        let mut recovered_shares: Vec<RecoveredShare> = Vec::new();
        for recovered_share_string in recovered_share_strings.iter() {
            recovered_shares.push(RecoveredShare::from_concat(
                &hex::decode(recovered_share_string).unwrap(),
            ));
        }
        assert_eq!(recover(recovered_shares).unwrap(), b"hello");
    }
}
