
## dark-crystal-web3-backup

Used by the secret owner to make and recover backups for [Dark Crystal Web3](https://web3.darkcrystal.pw).

Internally uses [dark-crystal-key-backup-rust](https://gitlab.com/dark-crystal-rust/dark-crystal-key-backup-rust) for secret sharing and encryption.

Try the [deployed version of the web front end](https://web3-backup.darkcrystal.pw) to see what this crate does.

### Usage

#### Command line interface

The CLI has commands to create and recover secrets as well as a `serve` command which serves the web-ui.

It expects to find the web-ui in `~/.local/share/dark-crystal-web3`

`dark-crystal-web3 help`

or from the repository:

`cargo run --bin cli -- help`

#### JS bindings for nodejs or the browser:

Clone the repository and do

`cd web3-backup-js-binding`

`wasm-pack build` or `wasm-pach build --target nodejs`

#### WASM Web front end using [yew](https://yew.rs/)

Install [trunk](https://trunkrs.dev/):

`cargo install --locked trunk`

`cd dark-crystal-web3-web-ui`
`trunk serve`

And open `0.0.0.0:8080` in a browser

