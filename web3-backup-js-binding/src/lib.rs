use dark_crystal_web3_core::secret::Secret;
use dark_crystal_web3_core::{RecoveredShare, SHARE_LENGTH};
use serde::{Deserialize, Serialize};
use serde_json::json;
use wasm_bindgen::prelude::*;

/// Parameters to the share function
#[derive(Serialize, Deserialize)]
struct ShareParams {
    public_keys: Vec<String>,
    secret: String,
    lookup_key: String,
}

/// Share function which takes parameters as a JSON object
#[wasm_bindgen]
pub fn share(params: &str) -> String {
    let share_params: ShareParams;
    match serde_json::from_str(&params) {
        Ok(s) => share_params = s,
        Err(_) => return error("Cannot parse json".to_string()),
    }

    let mut public_keys: Vec<[u8; 32]> = Vec::new();
    for pk in share_params.public_keys.iter() {
        if pk.len() != 64 {
            return error("Public keys must be 32 bytes".to_string());
        }
        match hex::decode(&pk) {
            Ok(pk_bytes) => public_keys.push(pk_bytes.try_into().unwrap()),
            Err(_) => return error("Cannot decode hex public key".to_string()),
        };
    }

    let secret = Secret::detect_from_string(share_params.secret)
        .to_bytes()
        .unwrap();
    let lookup_key = &share_params.lookup_key.as_bytes().to_vec();

    // "Valid".to_string()
    match dark_crystal_web3_core::share(public_keys, secret.to_vec(), lookup_key.to_vec()) {
        Ok(shares) => success(shares),
        Err(err) => error(format!("{:?}", err)),
    }
}

/// Parameters to the recover function
#[derive(Serialize, Deserialize)]
struct RecoverParams {
    shares: Vec<String>,
}

/// Recover function which takes parameters as a JSON object
#[wasm_bindgen]
pub fn recover(params: &str) -> String {
    let recover_params: RecoverParams;
    match serde_json::from_str(&params) {
        Ok(r) => recover_params = r,
        Err(_) => return error("Cannot decode JSON arguments".to_string()),
    }

    let mut shares: Vec<RecoveredShare> = Vec::new();
    for share in recover_params.shares.iter() {
        if share.len() < SHARE_LENGTH * 2 {
            return error("Share too short".to_string());
        }
        match hex::decode(&share) {
            Ok(share_bytes) => shares.push(RecoveredShare::from_concat(&share_bytes)),
            Err(_) => return error("Cannot decode hex share".to_string()),
        };
    }
    match dark_crystal_web3_core::recover(shares) {
        Ok(secret_raw) => match Secret::from_bytes(secret_raw) {
            Ok(secret) => {
                let secret_string = match secret {
                    Secret::Raw(bytes) => hex::encode(bytes),
                    Secret::Utf8(string) => string,
                    Secret::Bip39(string) => string,
                };

                success(secret_string)
            }
            // TODO include raw secret here
            Err(_) => error("Cannot decode secret".to_string()),
        },
        Err(recovery_error) => error(recovery_error.message),
    }
}

fn success(output: String) -> String {
    json!({ "Success": output }).to_string()
}

fn error(message: String) -> String {
    json!({ "Error": message }).to_string()
}
