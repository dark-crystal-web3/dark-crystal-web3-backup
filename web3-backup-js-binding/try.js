const dc = require('./pkg/web3_backup_js_binding')

function share (publicKeysHex, secret, lookupKey) {
  const output = JSON.parse(dc.share(JSON.stringify({
    public_keys: publicKeysHex,
    secret,
    lookup_key: lookupKey
  })))
  if (output.Error) throw new Error(output.Error)
  return output.Success
}

function recover (shares) {
  const output = JSON.parse(dc.recover(JSON.stringify({ shares })))
  if (output.Error) throw new Error(output.Error)
  return output.Success
}

console.log(share(
  [
    'cf61ca2d786bc57cff2455003862ee70ca64d0dddb8b6ae0d1927e81411bdd21',
    'd92d16240856d9fa136712942943e0869e7d7b93e8b3417ee4cf20946e042b0e'
  ],
  'spare human danger common patch pioneer security pond push purity wear crane',
  'Some lookup key'
))

console.log(recover([
  '02beb726fa3acfd0b148fd1b77da2e1daa6700272868049c6e14affb728da15ffd0c5f8359d724ed303748556778b1bf0171b9317773a614e21726aa25415d5f3a071f9c6f3b4c1d5eec62339ab7f8755212792d7fa36d4bf0b2',
  '0476f084048efb24226a330c7aef84007eac83a88ee129c193ea765a282f7c57780c5f8359d724ed303748556778b1bf0171b9317773a614e21726aa25415d5f3a071f9c6f3b4c1d5eec62339ab7f8755212792d7fa36d4bf0b2',
  '059c5a5dbfe39f7dc08547a713ca047aebf2b637291c1ad6d28cbdae4d452a3ad30c5f8359d724ed303748556778b1bf0171b9317773a614e21726aa25415d5f3a071f9c6f3b4c1d5eec62339ab7f8755212792d7fa36d4bf0b2'
]))

console.log(recover([
  '02b7fa36d4bf',
  '0476f084048efb24226a330c7aef84007eac83a88ee129c193ea765a282f7c57780c5f8359d724ed303748556778b1bf0171b9317773a614e21726aa25415d5f3a071f9c6f3b4c1d5eec62339ab7f8755212792d7fa36d4bf0b2',
  '059c5a5dbfe39f7dc08547a713ca047aebf2b637291c1ad6d28cbdae4d452a3ad30c5f8359d724ed303748556778b1bf0171b9317773a614e21726aa25415d5f3a071f9c6f3b4c1d5eec62339ab7f8755212792d7fa36d4bf0b2'
]))
