
JS Bindings to the share and recover functions for nodejs or the browser

Build with:
`wasm-pack build`

For nodejs:
`wasm-pack build --target nodejs`
