use crate::components::{recover::Recover, share::Share};
use yew::prelude::*;

pub enum Msg {
    SetTab(Tab),
}

#[derive(Debug)]
pub enum Tab {
    Share,
    Recover,
}

// TODO default trait
#[derive(Debug)]
pub struct App {
    current_tab: Tab,
}

impl Component for App {
    type Message = Msg;
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        App {
            current_tab: Tab::Share,
        }
    }

    fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::SetTab(tab) => {
                self.current_tab = tab;
            }
        }
        // re-render the page
        true
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let link = ctx.link();
        html! {
            <div>
            <div class="container mb-5">
                <ul class="nav nav-tabs">
                  <li class="nav-item">
                    <a class={
                        match &self.current_tab {
                            Tab::Share => { "nav-link active" },
                            _ => { "nav-link link-dark" },
                        }
                        }
                        onclick={ link.callback(|_| Msg::SetTab(Tab::Share)) }
                        aria-current="page">{ "Backup" }</a>
                  </li>
                  <li class="nav-item">
                    <a class={
                        match &self.current_tab {
                            Tab::Recover => { "nav-link active" },
                            _ => { "nav-link link-dark" },
                        }
                        }
                        onclick={ link.callback(|_| Msg::SetTab(Tab::Recover)) }
                        aria-current="page">{ "Recover" }</a>
                  </li>
                </ul>
                { match &self.current_tab {
                    Tab::Share => html! {<Share />},
                    Tab::Recover => html! {<Recover />},
                } }
            </div>
            <footer class="text-center text-muted fixed-bottom my-2">
              <a href="https://darkcrystal.pw" class="text-reset">{"darkcrystal.pw"}</a>
            </footer>
        </div>
        }
    }
}
