//! Validation for form text input fields
use hex;

/// Validity types
#[derive(Debug, PartialEq)]
pub enum Validity {
    Valid,
    Empty,
    BadHex,
    WrongLength,
}

/// Representation of a text input field and its validity
#[derive(Debug)]
pub struct InputItem {
    pub value: String,
    pub validity: Validity,
}

impl InputItem {
    pub fn new() -> Self {
        Self {
            value: String::new(),
            validity: Validity::Empty,
        }
    }

    pub fn set(&mut self, value: &str, is_hex: bool) {
        self.value = value.to_string();
        self.validity = match value.len() {
            0 => Validity::Empty,
            _ => match is_hex {
                true => match hex::decode(&value) {
                    Ok(_) => Validity::Valid,
                    Err(_) => Validity::BadHex,
                },
                false => Validity::Valid,
            },
        };
    }
}
