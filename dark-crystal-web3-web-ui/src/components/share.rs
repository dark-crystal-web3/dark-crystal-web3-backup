use crate::components::{
    icons::{icon, Icon},
    input_item::{InputItem, Validity},
    text_input::TextInput,
};
use dark_crystal_web3_core::{default_threshold, secret::Secret, share};
use hex;
use qrcodegen::{QrCode, QrCodeEcc};
use web_sys::Navigator;
use yew::html::Scope;
use yew::prelude::*;

pub enum Msg {
    SetSecret(String),
    SetLookupKey(String),
    SetPk(usize, String),
    AddPk,
    CreateShares,
    CreateTestData,
    Reset,
    CopyLinkToClipboard,
}

// TODO default trait
#[derive(Debug)]
pub struct Share {
    secret: InputItem,
    lookup_key: InputItem,
    public_keys: Vec<InputItem>,
    threshold_text: Option<Html>,
    output: Option<String>,
    error: String,
}

impl Component for Share {
    type Message = Msg;
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        Self {
            secret: InputItem::new(),
            lookup_key: InputItem::new(),
            public_keys: vec![InputItem::new(), InputItem::new()],
            threshold_text: Option::None,
            output: Option::None,
            error: String::new(),
        }
    }

    fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::SetSecret(next_secret) => {
                self.secret.set(&next_secret, false);
            }
            Msg::SetLookupKey(next_lookup_key) => {
                self.lookup_key.set(&next_lookup_key, false);
            }
            Msg::SetPk(idx, next_pk) => {
                self.public_keys[idx].set(&next_pk, true);
                if next_pk.len() > 0 && next_pk.len() != 64 {
                    self.public_keys[idx].validity = Validity::WrongLength;
                }
            }
            Msg::AddPk => {
                if self.public_keys.len() < 255 {
                    self.public_keys.push(InputItem::new());
                }
            }
            Msg::CreateShares => match self.can_share() {
                true => {
                    let mut public_keys: Vec<[u8; 32]> = Vec::new();
                    for pk in self
                        .public_keys
                        .iter()
                        .filter(|pk| pk.validity != Validity::Empty)
                    {
                        public_keys.push(hex::decode(&pk.value).unwrap().try_into().unwrap());
                    }

                    let secret = Secret::detect_from_string(self.secret.value.clone())
                        .to_bytes()
                        .unwrap();

                    let lookup_key = &self.lookup_key.value.as_bytes().to_vec();
                    let n = public_keys.len().clone();
                    let m = default_threshold(n.try_into().unwrap()) as usize;

                    match share(public_keys, secret, lookup_key.to_vec()) {
                        Ok(shares) => {
                            self.threshold_text = Some(html! {
                                <div>
                                <p>
                                { for vec![1; m].iter().map(|_| icon(Icon::PersonFill)) }
                                { for vec![1; n - m].iter().map(|_| icon(Icon::Person)) }
                                </p>
                                <p>{ format!("Created {} shares. {} shares are needed to recover.", n, m) }</p>
                                </div>
                            });
                            self.output = Some(shares);
                            self.secret = InputItem::new();
                            self.lookup_key = InputItem::new();
                            self.public_keys = vec![InputItem::new(), InputItem::new()];
                        }
                        Err(err) => self.error = format!("{:?}", err),
                    }
                }
                false => {
                    self.error = String::from("Cannot share");
                }
            },
            Msg::CreateTestData => {
                // TODO use .set
                self.secret = InputItem {
                    value: String::from(
                        "spare human danger common patch pioneer security pond push purity wear crane",
                    ),
                    validity: Validity::Valid,
                };
                self.lookup_key = InputItem {
                    value: String::from("Some lookup key"),
                    validity: Validity::Valid,
                };
                self.public_keys = vec![
                    InputItem {
                        value: String::from(
                            "cf61ca2d786bc57cff2455003862ee70ca64d0dddb8b6ae0d1927e81411bdd21",
                        ),
                        validity: Validity::Valid,
                    },
                    InputItem {
                        value: String::from(
                            "d92d16240856d9fa136712942943e0869e7d7b93e8b3417ee4cf20946e042b0e",
                        ),
                        validity: Validity::Valid,
                    },
                    InputItem {
                        value: String::from(
                            "f326c7c66fd5e2af1dc535d3aac20cf2aec2ac22cd92de0e57a66c80a7a9ef50",
                        ),
                        validity: Validity::Valid,
                    },
                ];
            }
            Msg::CopyLinkToClipboard => {
                if let Some(link) = &self.output {
                    let navigator: Navigator = web_sys::window().unwrap().navigator();
                    if let Some(clipboard) = navigator.clipboard() {
                        let _ = clipboard.write_text(&link.clone());
                    }
                }
            }
            Msg::Reset => {
                self.output = Option::None;
                self.threshold_text = Option::None;
            }
        }
        // Re-render the page
        true
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let link = ctx.link();
        let on_change = link.callback(Msg::SetSecret);
        match &self.output {
            None => html! {
                <div class="entry">
                    <div>
                      <label class="form-label">{"Secret"}</label>
                      <TextInput {on_change} value={self.secret.value.clone()} placeholder={"Enter a secret"} title={ Some("Secret can be plain text, hex, or a seed phrase") }/>
                    </div>

                    <div>
                      <label class="form-label">{"Lookup key"}</label>
                      <TextInput on_change={ ctx.link().callback(Msg::SetLookupKey) } value={self.lookup_key.value.clone()} placeholder={"Identifier for the secret"} />
                    </div>

                    <div class="mb-2 p-2">
                      <label class="form-label">{"Public keys of recovery partners"}</label>
                      <ol>
                        { for self.public_keys.iter().enumerate().map(|pk| self.view_pk(pk, ctx.link()) ) }
                        <li>
                            <button class="btn btn-outline-dark btn-sm" onclick={link.callback(|_| Msg::AddPk)} title="Add another public key">{ "+" }</button>
                        </li>
                      </ol>
                    </div>

                    <div>
                      <button class="btn btn-outline-dark" onclick={link.callback(|_| Msg::CreateShares)} disabled={ !self.can_share() } title={ "Create an encrypted share set" }>
                        { "Create Shares " }
                        { icon(Icon::Gem) }
                      </button>
                      { " " }
                      <button class="btn btn-outline-dark mx-1" onclick={link.callback(|_| Msg::CreateTestData)} title={ "Fill the boxes with test data" }>{ "Add test data" }</button>
                    </div>
                    <div>
                      <p>{self.error.clone()}</p>
                    </div>
                </div>
            },
            Some(output) => html! {
                <div class="entry">
                    <h4 class="my-2">
                      { icon(Icon::Gem) }
                      { " Backup created" }
                    </h4>
                    { match &self.threshold_text {
                        None => html!{},
                        Some(text) => text.clone(),
                    } }
                    <div class="card mb-4">
                       <div class="card-header">
                         <h5>
                            { "Use this link to publish the encryped shares" }
                            <button class="btn btn-sm btn-outline-dark mx-3" title="Copy link to clipboard" onclick={link.callback(|_| Msg::CopyLinkToClipboard)}>
                              { icon(Icon::Clipboard) }
                            </button>
                         </h5>
                          </div>
                            <div class="card-body">
                      <a href={output.clone()} target="_blank">
                        <code class="text-reset">{output.clone()}</code>
                      </a>
                    </div>
                    </div>
                    { qr(&output) }
                    <div>
                      <button class="btn btn-outline-dark" onclick={link.callback(|_| Msg::Reset)} title="Enter a new secret">
                        { "Reset" }
                      </button>
                    </div>
                </div>
            },
        }
    }
}

impl Share {
    fn view_pk(&self, (idx, pk): (usize, &InputItem), link: &Scope<Self>) -> Html {
        let on_change = link.callback(move |val| Msg::SetPk(idx, val));
        html! {
            <li>
                <TextInput {on_change} value={pk.value.clone()} placeholder={"Recovery partner's encryption public key"} />
                <div class="form-text text-danger">{
                    match pk.validity {
                      Validity::BadHex  => { "Not valid hex" },
                      Validity::WrongLength => { "Must be 32 bytes" },
                      _ => { "" },
                    }
                }</div>
            </li>
        }
    }

    fn can_share(&self) -> bool {
        self.secret.validity == Validity::Valid
            && self
                .public_keys
                .iter()
                .filter(|pk| pk.validity != Validity::Empty)
                .all(|pk| pk.validity == Validity::Valid)
            && self
                .public_keys
                .iter()
                .filter(|pk| pk.validity != Validity::Empty)
                .count()
                > 1
    }
}

fn qr(data: &str) -> Html {
    let qr = QrCode::encode_text(data, QrCodeEcc::High).unwrap();
    to_svg(&qr, 1)
}

fn to_svg(qr: &QrCode, border: i32) -> Html {
    assert!(border >= 0, "Border must be non-negative");
    let dimension = qr
        .size()
        .checked_add(border.checked_mul(2).unwrap())
        .unwrap();
    html! {
      <svg xmlns="http://www.w3.org/2000/svg" class="img-fluid" version="1.1" width={"800px"} viewBox={format!("0 0 {0} {0}", dimension)} stroke={"none"}>
          <rect width="100%" height="100%" fill="#FFFFFF" />
          <path d={ create_path(qr, border) } fill="#000000"/>
      </svg>
    }
}

fn create_path(qr: &QrCode, border: i32) -> String {
    let mut result = String::new();
    for y in 0..qr.size() {
        for x in 0..qr.size() {
            if qr.get_module(x, y) {
                if x != 0 || y != 0 {
                    result += " ";
                }
                result += &format!("M{},{}h1v1h-1z", x + border, y + border);
            }
        }
    }
    result
}
