use crate::components::{
    icons::{icon, Icon},
    input_item::{InputItem, Validity},
    text_input::TextInput,
};
use dark_crystal_web3_core::secret::Secret;
use dark_crystal_web3_core::{recover, RecoveredShare};
use hex;
use web_sys::Navigator;
use yew::html::Scope;
use yew::prelude::*;

const MIN_SHARE_LENGTH: usize = 33 + 16;

pub enum Msg {
    SetShare(usize, String),
    AddShare,
    TryToRecover,
    AddTestData,
    CopySecretToClipboard,
}

// TODO default trait
#[derive(Debug)]
pub struct Recover {
    shares: Vec<InputItem>,
    secret: Option<String>,
    error: String,
}

impl Component for Recover {
    type Message = Msg;
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        Self {
            shares: vec![InputItem::new(), InputItem::new()],
            secret: Option::None,
            error: String::new(),
        }
    }

    fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::SetShare(idx, next_share) => {
                self.secret = Option::None;
                self.error = String::new();
                self.shares[idx].set(&next_share, true);
                if next_share.len() > 0 && next_share.len() < MIN_SHARE_LENGTH {
                    self.shares[idx].validity = Validity::WrongLength;
                }
            }
            Msg::AddShare => {
                self.shares.push(InputItem::new());
            }
            Msg::AddTestData => {
                self.secret = Option::None;
                self.error = String::new();
                self.shares = vec![
                    InputItem {
                        value: String::from("02beb726fa3acfd0b148fd1b77da2e1daa6700272868049c6e14affb728da15ffd0c5f8359d724ed303748556778b1bf0171b9317773a614e21726aa25415d5f3a071f9c6f3b4c1d5eec62339ab7f8755212792d7fa36d4bf0b2"),
                        validity: Validity::Valid,
                    },
                    InputItem {
                        value: String::from("0476f084048efb24226a330c7aef84007eac83a88ee129c193ea765a282f7c57780c5f8359d724ed303748556778b1bf0171b9317773a614e21726aa25415d5f3a071f9c6f3b4c1d5eec62339ab7f8755212792d7fa36d4bf0b2"),
                        validity: Validity::Valid,
                    },
                    InputItem {
                        value: String::from("059c5a5dbfe39f7dc08547a713ca047aebf2b637291c1ad6d28cbdae4d452a3ad30c5f8359d724ed303748556778b1bf0171b9317773a614e21726aa25415d5f3a071f9c6f3b4c1d5eec62339ab7f8755212792d7fa36d4bf0b2"),
                        validity: Validity::Valid,
                    },
                ];
            }
            Msg::TryToRecover => match self.can_recover() {
                true => {
                    let mut shares: Vec<RecoveredShare> = Vec::new();
                    for share in self
                        .shares
                        .iter()
                        .filter(|share| share.validity != Validity::Empty)
                    {
                        shares.push(RecoveredShare::from_concat(
                            &hex::decode(&share.value).unwrap(),
                        ));
                    }
                    match recover(shares) {
                        Ok(secret_raw) => match Secret::from_bytes(secret_raw) {
                            Ok(secret) => {
                                let secret_string = match secret {
                                    Secret::Raw(bytes) => hex::encode(bytes),
                                    Secret::Utf8(string) => string,
                                    Secret::Bip39(string) => string,
                                };

                                self.secret = Some(secret_string);
                                self.shares = vec![InputItem::new(), InputItem::new()];
                                self.error = String::new();
                            }
                            Err(_) => self.error = "Cannot decode secret".to_string(),
                        },
                        Err(recovery_error) => self.error = recovery_error.message,
                    }
                }
                false => self.error = String::from("Cannot Recover"),
            },
            Msg::CopySecretToClipboard => {
                if let Some(secret) = &self.secret {
                    let navigator: Navigator = web_sys::window().unwrap().navigator();
                    if let Some(clipboard) = navigator.clipboard() {
                        let _ = clipboard.write_text(&secret.clone());
                    }
                }
            }
        }
        // Re-render the page
        true
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let link = ctx.link();
        html! {
            <div class="entry">
                <div>
                  <label class="form-label">{"Shares from recovery partners"}</label>
                  <ol>
                    { for self.shares.iter().enumerate().map(|share| self.view_share(share, ctx.link()) ) }
                    <li>
                        <button class="btn btn-outline-dark btn-sm" onclick={link.callback(|_| Msg::AddShare)} title="Add another Share">{ "+" }</button>
                    </li>
                  </ol>
                </div>
                <div>
                    <button class="btn btn-outline-dark" onclick={link.callback(|_| Msg::TryToRecover)} disabled={ !self.can_recover() } title={ "Attempt to recover secret" }>
                    { "Recover " }
                    { icon(Icon::Key) }
                    </button>
                    { " " }
                    <button class="btn btn-outline-dark mx-1" onclick={link.callback(|_| Msg::AddTestData)} title={ "Add some test shares" }>{ "Add test data" }</button>
                </div>
                <div>
                  <p>{self.error.clone()}</p>
                </div>
                { match &self.secret {
                    None => html! {},
                    Some(secret) => html! {
                      <div>
                          <div class="card">
                            <div class="card-header">
                            <h5>
                              { icon(Icon::EnvelopeHeart) }
                              {" Secret recovered"}
                            <button class="btn btn-sm btn-outline-dark mx-3" title="Copy secret to clipboard" onclick={link.callback(|_| Msg::CopySecretToClipboard)}>
                              { icon(Icon::Clipboard) }
                            </button>
                      </h5>
                          </div>
                            <div class="card-body">
                              <code class="text-reset">{secret.clone()}</code>
                            </div>
                          </div>
                      </div>
                    }
                                     } }
            </div>
        }
    }
}

impl Recover {
    fn view_share(&self, (idx, share): (usize, &InputItem), link: &Scope<Self>) -> Html {
        let on_change = link.callback(move |val| Msg::SetShare(idx, val));
        html! {
            <li>
                <TextInput {on_change} value={share.value.clone()} placeholder={"A decrypted share"} />
                <div class="form-text text-danger">{
                    match share.validity {
                      Validity::BadHex => { "Not valid hex".to_string() },
                      Validity::WrongLength => { format!("Must be at least {} bytes", MIN_SHARE_LENGTH) }
                      _ => { "".to_string() },
                    }
                }</div>
            </li>
        }
    }

    fn can_recover(&self) -> bool {
        self.shares
            .iter()
            .filter(|share| share.validity != Validity::Empty)
            .all(|share| share.validity == Validity::Valid)
            && self
                .shares
                .iter()
                .filter(|share| share.validity != Validity::Empty)
                .count()
                > 1
    }
}
