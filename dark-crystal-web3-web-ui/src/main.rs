#![recursion_limit = "256"]

mod app;
mod components;

use app::App;

fn main() {
    yew::start_app::<App>();
    wasm_logger::init(wasm_logger::Config::default());
}
